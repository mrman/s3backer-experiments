#!/usr/bin/env -S bash -eu
#
# This file installs nbdkit

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

############################
# Filesystem setup & Mount #
############################

S3BACKER_MOUNTS_DIR=/mnt/s3backer
FSTAB_MOUNT_DIR=${S3BACKER_MOUNTS_DIR}/${MOUNT_NAME}

echo -e "===== fs setup & mount =====";

# build the filesystem and mount it
FS_SETUP_AND_MOUNT_LOCK=/etc/experiment/progress/mount-${MOUNT_NAME}-fs-setup-and-mount.lock
if [ -f "${FS_SETUP_AND_MOUNT_LOCK}" ]; then
    echo -e "[info] detected install lock @ [${FS_SETUP_AND_MOUNT_LOCK}], skipping fs setup & mount...";
    exit 0;
fi

echo "[info] creating filesystem on [${MOUNT_DEVICE_PATH}], if necessary...";
if ! blkid ${MOUNT_DEVICE_PATH} | grep "ext4" ; then
  mkfs.ext4 ${MOUNT_DEVICE_PATH} || true;
else
  echo -e "[info] fs (ext4) already exists on [${MOUNT_DEVICE_PATH}]!";
fi

echo "[info] creating directory for mount [${FSTAB_MOUNT_DIR}]..."
mkdir -p ${FSTAB_MOUNT_DIR};

echo "[info] checking for fstab mount..."
if ! grep "${FSTAB_MOUNT_DIR}" /etc/fstab; then
  echo "[info] no existing mount, adding one...";
  cat <<EOF >> /etc/fstab
# managed by s3backer via s3b-mounts-${MOUNT_NAME}.service
${MOUNT_DEVICE_PATH}  ${FSTAB_MOUNT_DIR}   ext4   _netdev,discard,noatime,nodiratime,x-systemd.requires=s3b-mounts-${MOUNT_NAME}.service 0 0
EOF
else
  echo "[info] mount already exists!";
fi

echo "[info] force-remounting...";
umount ${MOUNT_DEVICE_PATH} || true;
mount ${MOUNT_DEVICE_PATH};
echo "[info] mount successful!";

echo "[info] writing lock file @ [${FS_SETUP_AND_MOUNT_LOCK}]";
date > ${FS_SETUP_AND_MOUNT_LOCK};
