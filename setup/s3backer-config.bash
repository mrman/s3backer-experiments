#!/usr/bin/env -S bash -eu
#
# This file installs nbdkit

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

##########################
# S3Backer Configuration #
##########################

echo -e "===== S3backer config =====";

S3BACKER_MOUNT_CONFIG_DIR=/home/$USER/s3backer/mounts/${MOUNT_NAME}
S3BACKER_MOUNT_OPTIONS_FILE_PATH=${S3BACKER_MOUNT_CONFIG_DIR}/s3backer.options.conf

echo -e "[info] creating directories for s3backer mount [${MOUNT_NAME}]...";
mkdir -p ${S3BACKER_MOUNT_CONFIG_DIR};

echo -e "[info] writing out folders and credentials for s3backer mount [${MOUNT_NAME}]...";
# create access file for use by s3backer
echo -e "${S3_ACCESS_KEY_ID}:${S3_SECRET_ACCESS_KEY}" > ${S3BACKER_MOUNT_CONFIG_DIR}/s3b.access-file.secret;
# create encryption key
echo -e "${S3B_ENCRYPTION_KEY}" > ${S3BACKER_MOUNT_CONFIG_DIR}/encryption-key.secret;

# Set  s3backer with always-on locally mounted drive
MOUNT_CONFIG_LOCK=/etc/experiment/progress/mount-${MOUNT_NAME}-config.lock
if [ -f "${MOUNT_CONFIG_LOCK}" ]; then
    echo -e "[info] detected install lock @ [${MOUNT_CONFIG_LOCK}], skipping s3backer config...";
    exit 0;
fi

echo -e "[info] using s3 region [${S3_REGION}]";
echo -e "[info] using s3 base url [${S3_BASE_URL}]";
echo -e "[info] using bucket prefix [${MOUNT_S3_BUCKET_PREFIX}]";

# Set up S3backer mount
cat <<EOF > ${S3BACKER_MOUNT_OPTIONS_FILE_PATH}
# s3backer options

# Sizing
--size=1t
--force
--blockSize=256k

# S3
--region=${S3_REGION}
--baseURL=${S3_BASE_URL}/
--prefix=${MOUNT_S3_BUCKET_PREFIX}

# Credentials
--accessFile=${S3BACKER_MOUNT_CONFIG_DIR}/s3b.access-file.secret

# List blocks on startup
--listBlocks
--listBlocksThreads=50

# Encryption
--ssl
--encrypt
--passwordFile=${S3BACKER_MOUNT_CONFIG_DIR}/encryption-key.secret

# Block cache
--blockCacheSize=${MOUNT_S3BACKER_BLOCK_CACHE_SIZE}
--blockCacheFile=${S3BACKER_MOUNT_CONFIG_DIR}/block-cache
--blockCacheWriteDelay=15000
--blockCacheThreads=4
--blockCacheRecoverDirtyBlocks
--blockCacheNumProtected=1000

# Misc
--timeout=90
EOF

echo -e "[info] successfully wrote s3backer options to [${S3BACKER_MOUNT_OPTIONS_FILE_PATH}]";

echo "[info] writing lock file @ [${MOUNT_CONFIG_LOCK}]";
date > ${MOUNT_CONFIG_LOCK};
