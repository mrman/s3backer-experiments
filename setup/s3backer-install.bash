#!/usr/bin/env -S bash -eu
#
# This file installs s3backer

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

####################
# S3Backer Install #
####################

S3BACKER_RELEASE_DOWNLOAD_URL=https://s3.amazonaws.com/archie-public/s3backer/s3backer-${S3BACKER_RELEASE_VERSION}.tar.gz
S3BACKER_LIB_DIR=/var/lib/s3backer
S3BACKER_EXPECTED_TARBALL_PATH=${S3BACKER_LIB_DIR}/s3backer-${S3BACKER_RELEASE_VERSION}.tar.gz;
S3BACKER_EXPECTED_UNZIPPED_DIR=${S3BACKER_LIB_DIR}/s3backer-${S3BACKER_RELEASE_VERSION}/

echo -e "===== S3backer install =====";

# Install s3backer
S3BACKER_INSTALL_LOCK=/etc/experiment/progress/s3backer-${S3BACKER_RELEASE_VERSION}-install.lock
if [ -f "${S3BACKER_INSTALL_LOCK}" ]; then
    echo -e "[info] detected install lock @ [${S3BACKER_INSTALL_LOCK}], skipping s3backer install...";
    exit 0;
fi

echo "[info] failed to detect s3backer install lock, installing s3backer...";
apt update -y && apt install -y libcurl4-openssl-dev libfuse-dev libexpat1-dev libssl-dev zlib1g-dev pkg-config autoconf make build-essential nbdkit libnbd-dev;

echo "[info] creating lib dir...";
mkdir -p ${S3BACKER_LIB_DIR};

echo "[info] downloading the source code";
curl --output ${S3BACKER_EXPECTED_TARBALL_PATH} -LO ${S3BACKER_RELEASE_DOWNLOAD_URL};
cd ${S3BACKER_LIB_DIR} && tar -xf ${S3BACKER_EXPECTED_TARBALL_PATH};

echo "[info] building the project...";
pushd ${S3BACKER_EXPECTED_UNZIPPED_DIR};
./configure;
make
make install;
popd;

echo "[info] configuring fuse...";
sudo sh -c 'echo user_allow_other >> /etc/fuse.conf';

echo "[info] writing lock file @ [${S3BACKER_INSTALL_LOCK}]";
date > ${S3BACKER_INSTALL_LOCK};
