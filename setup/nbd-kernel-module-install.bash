#!/usr/bin/env -S bash -eu
#
# This file ensures the nbd kernel module is installed nad present

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

#####################
# NBD Kernel Module #
#####################

echo -e "===== nbd kernel module install =====";

# Set up the nbd kernel module
NBD_KERNEL_MODULE_INSTALL_LOCK=/etc/experiment/progress/nbd-kernel-module-install.lock
if [ -f "${NBD_KERNEL_MODULE_INSTALL_LOCK}" ]; then
    echo -e "[info] detected install lock @ [${NBD_KERNEL_MODULE_INSTALL_LOCK}], skipping nbd kernel module install...";
    exit 0;
fi

echo "[info] enabling kernel module...";
modprobe nbd;

echo "[info] persisting nbd as boot-time loaded module";
echo "nbd" > /etc/modules-load.d/nbd.conf;

echo "[info] writing lock file @ [${NBD_KERNEL_MODULE_INSTALL_LOCK}]";
date > ${NBD_KERNEL_MODULE_INSTALL_LOCK};
