#!/usr/bin/env -S bash -eu
#
# This file installs libnbd

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

##################
# libnbd install #
##################

#LIBNBD_RELEASE_VERSION=1.14.2 # should be provided via ENV
#LIBNBD_RELEASE_MAJOR_MINOR_VERSION=1.14 # should be provided via ENV
LIBNBD_RELEASE_DOWNLOAD_URL=https://download.libguestfs.org/libnbd/${LIBNBD_RELEASE_MAJOR_MINOR_VERSION}-stable/libnbd-${LIBNBD_RELEASE_VERSION}.tar.gz
LIBNBD_LIB_DIR=/var/lib/libnbd
LIBNBD_EXPECTED_TARBALL_PATH=${LIBNBD_LIB_DIR}/libnbd-${LIBNBD_RELEASE_VERSION}.tar.gz;
LIBNBD_EXPECTED_UNZIPPED_DIR=${LIBNBD_LIB_DIR}/libnbd-${LIBNBD_RELEASE_VERSION}/

echo -e "===== libnbd install =====";

# Install libnbd (if necessary)
LIBNBD_INSTALL_LOCK=${CONFIG_PROGRESS_DIR}/libnbd-${LIBNBD_RELEASE_VERSION}-install.lock
if [ -f "${LIBNBD_INSTALL_LOCK}" ]; then
  echo -e "[info] detected install lock @ [${LIBNBD_INSTALL_LOCK}], skipping libnbd install...";
  exit 0;
fi

echo "[info] creating lib dir...";
mkdir -p ${LIBNBD_LIB_DIR};

echo "[info] downloading & unzipping the source code";
curl --output ${LIBNBD_EXPECTED_TARBALL_PATH} -LO ${LIBNBD_RELEASE_DOWNLOAD_URL};
cd ${LIBNBD_LIB_DIR} && tar -xf ${LIBNBD_EXPECTED_TARBALL_PATH};

echo "[info] building the project...";
pushd ${LIBNBD_EXPECTED_UNZIPPED_DIR};
./configure;
make
make install;
popd;

echo "[info] installing nbd-client...";
apt install nbd-client;

echo "[info] writing lock file @ [${LIBNBD_INSTALL_LOCK}]";
date > ${LIBNBD_INSTALL_LOCK};
