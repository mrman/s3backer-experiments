#!/usr/bin/env -S bash -eu
#
# This file creates and installs the s3backer-mount service for the given mount

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

###########################
# systemd service install #
###########################

echo -e "===== systemd service setup =====";

S3BACKER_MOUNT_CONFIG_DIR=/home/$USER/s3backer/mounts/${MOUNT_NAME}
S3BACKER_MOUNT_OPTIONS_FILE_PATH=${S3BACKER_MOUNT_CONFIG_DIR}/s3backer.options.conf

MOUNT_SYSTEMD_SERVICE_PATH=/etc/systemd/system/s3b-mounts-${MOUNT_NAME}.service

# Set  s3backer with always-on locally mounted drive
MOUNT_SYSTEMD_SERVICE_SETUP_LOCK=/etc/experiment/progress/mount-${MOUNT_NAME}-systemd-setup.lock
if [ -f "${MOUNT_SYSTEMD_SERVICE_SETUP_LOCK}" ]; then
    echo -e "[info] detected install lock @ [${MOUNT_SYSTEMD_SERVICE_SETUP_LOCK}], skipping setup for persistent mount...";
    exit 0;
fi

echo -e "[info] using s3 bucket [${MOUNT_S3_BUCKET}]";

echo -e "[info] no install install lock @ [${MOUNT_SYSTEMD_SERVICE_SETUP_LOCK}], setting up persistent mount...";
# TODO: setup persistent mount
cat <<EOF > ${MOUNT_SYSTEMD_SERVICE_PATH}
[Unit]
Description=s3backer running in NBD mode
After=network-online.target
Wants=network-online.target
Documentation=https://github.com/archiecobbs/s3backer

[Install]
WantedBy=multi-user.target

[Service]
Type=forking
ExecStartPre=mkdir -p /var/run/s3backer-nbd
ExecStart=/usr/bin/s3backer --nbd --configFile=${S3BACKER_MOUNT_OPTIONS_FILE_PATH} ${MOUNT_S3_BUCKET} ${MOUNT_DEVICE_PATH}
ExecStop=/usr/sbin/nbd-client -d ${MOUNT_DEVICE_PATH}

# Security hardening
ProtectSystem=full
##ProtectHome=read-only # mounts will be based on the home directory
ProtectHostname=true
ProtectClock=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectKernelLogs=true
ProtectControlGroups=true
RestrictRealtime=true

EOF
echo -e "[info] finished setting up systemd service for mount [${MOUNT_NAME}]";

echo "[info] enabling mount service...";
systemctl daemon-reload;
systemctl enable s3b-mounts-${MOUNT_NAME}.service;
systemctl restart s3b-mounts-${MOUNT_NAME}.service;

echo "[info] checking whether service is running properly (after waiting 5s)...";
sleep ${MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS};
systemctl is-active --quiet service && (echo -e "[error] s3b-mounts-${MOUNT_NAME} service failed to start -- please check the logs" && exit 1);

echo "[info] writing lock file @ [${MOUNT_SYSTEMD_SERVICE_SETUP_LOCK}]";
date > ${MOUNT_SYSTEMD_SERVICE_SETUP_LOCK};
