#!/usr/bin/env -S bash -eu
#
# This file installs nbdkit

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

##################
# NBDKit install #
##################

#NBDKIT_RELEASE_MAJOR_MINOR_VERSION=1.32 # should be provided via ENV
#NBDKIT_RELEASE_VERSION=1.32.5 # should be provided via ENV
NBDKIT_RELEASE_DOWNLOAD_URL=https://download.libguestfs.org/nbdkit/${NBDKIT_RELEASE_MAJOR_MINOR_VERSION}-stable/nbdkit-${NBDKIT_RELEASE_VERSION}.tar.gz
NBDKIT_LIB_DIR=/var/lib/nbdkit
NBDKIT_EXPECTED_TARBALL_PATH=${NBDKIT_LIB_DIR}/nbdkit-${NBDKIT_RELEASE_VERSION}.tar.gz;
NBDKIT_EXPECTED_UNZIPPED_DIR=${NBDKIT_LIB_DIR}/nbdkit-${NBDKIT_RELEASE_VERSION}/

echo -e "===== NBDKit install =====";

# Install nbdkit
NBDKIT_INSTALL_LOCK=${CONFIG_PROGRESS_DIR}/nbdkit-${NBDKIT_RELEASE_VERSION}-install.lock
if [ -f "${NBDKIT_INSTALL_LOCK}" ]; then
  echo -e "[info] detected install lock @ [${NBDKIT_INSTALL_LOCK}], skipping nbdkit install...";
  exit 0;
fi;

echo "[info] failed to detect nbdkit install lock, installing nbdkit...";
apt update -y && apt install -y libcurl4-openssl-dev libfuse-dev libexpat1-dev libssl-dev zlib1g-dev pkg-config autoconf make build-essential nbdkit libnbd-dev;

echo "[info] creating lib dir...";
mkdir -p ${NBDKIT_LIB_DIR};

echo "[info] downloading & unzipping the source code";
curl --output ${NBDKIT_EXPECTED_TARBALL_PATH} -LO ${NBDKIT_RELEASE_DOWNLOAD_URL};
cd ${NBDKIT_LIB_DIR} && tar -xf ${NBDKIT_EXPECTED_TARBALL_PATH};

echo "[info] building the project...";
pushd ${NBDKIT_EXPECTED_UNZIPPED_DIR};
./configure;
make
make install;
popd;

echo "[info] writing lock file @ [${NBDKIT_INSTALL_LOCK}]";
date > ${NBDKIT_INSTALL_LOCK};
