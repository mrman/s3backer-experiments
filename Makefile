# Makefile preamble (https://tech.davis-hansson.com/p/make/)
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
	$(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

ifndef VERBOSE
MAKEFLAGS += --no-print-directory
endif

.PHONY: all check-mount-name \
# Setup
>				setup check-mount-name copy-setup-scripts nbdkit-install libnbd-install nbd-kernel-module-install \
>				s3backer-install s3backer-config s3backer-mount-svc-install fs-setup \
# Reset
>				reset check-mount-name fs-setup-reset s3backer-mount-svc-install-reset \
>				s3backer-config-reset s3backer-install-reset \
>				nbd-kernel-module-install-reset libnbd-install-reset nbdkit-install-reset \
# Benchmark
>				bench copy-benchmark-scripts sqlite-bench-install sqlite-bench-run \
>				bench-reset sqlite-bench-run-reset sqlite-bench-install-reset

SCP ?= scp
SSH ?= ssh
SSH_TARGET ?= root@s3backer-sqlite.experiments.vadosware.io

B2_KEY_PATH ?= $(shell cat ../../../../../secrets/experiments/s3backer-plus-sqlite/B2_KEY.secret)
B2_ACCOUNT_ID_PATH ?= $(shell cat ../../../../../secrets/experiments/s3backer-plus-sqlite/B2_ACCOUNT_ID.secret)
ENCRYPTION_KEY_PATH ?= $(shell cat ../../../../../secrets/experiments/s3backer-plus-sqlite/encryption-key.secret)

LIBNBD_RELEASE_VERSION ?= 1.14.2
LIBNBD_RELEASE_MAJOR_MINOR_VERSION ?= 1.14
NBDKIT_RELEASE_VERSION ?= 1.32.5
NBDKIT_RELEASE_MAJOR_MINOR_VERSION ?= 1.32
S3BACKER_RELEASE_VERSION ?= 2.0.2

S3_REGION ?= eu-central-003
S3_BASE_URL ?= https://s3.eu-central-003.backblazeb2.com

# REMOTE configuration base directory
REMOTE_CONFIG_BASE_DIR=/etc/s3backer
REMOTE_SCRIPT_DIR ?= ${REMOTE_CONFIG_BASE_DIR}/scripts
REMOTE_CONFIG_PROGRESS_DIR=${REMOTE_CONFIG_BASE_DIR}/progress

# NOTE: Mount information is specific/likely to change from run to run
MOUNT_NAME ?= main

MOUNT_S3_BUCKET ?= experiment-s3backer-sqlite
MOUNT_S3_BUCKET_PREFIX ?= mounts/$(MOUNT_NAME)
MOUNT_DEVICE_PATH ?= /dev/nbd0
MOUNT_S3BACKER_BLOCK_CACHE_SIZE ?= 20000

MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS ?= 5

SQLITE_BENCH_LOCAL_DISK_DIR ?= /home/root
SQLITE_BENCH_S3BACKER_WITH_BLOCK_CACHE_DIR ?= /mnt/s3backer/cached
SQLITE_BENCH_S3BACKER_WITHOUT_BLOCK_CACHE_DIR ?= /mnt/s3backer/sync

all:
>	@echo "[info] setting up s3backer with block cache (MOUNT_NAME='cached')..."
>	@MOUNT_NAME=cached MOUNT_DEVICE_PATH=/dev/nbd1 $(MAKE) setup
>	@echo "[info] setting up s3backer without block cache (MOUNT_NAME='sync')..."
>	@MOUNT_NAME=sync MOUNT_DEVICE_PATH=/dev/nbd2 MOUNT_S3BACKER_BLOCK_CACHE_SIZE=0 $(MAKE) setup
>	@echo "[info] running benchmark..."
>	$(MAKE) bench

check-mount-name:
ifeq (,$(MOUNT_NAME))
	$(error "MOUNT_NAME not set")
endif

##################
# S3Backer Setup #
##################

## Set up a mount by running the script on the remote servers
setup:  check-mount-name copy-setup-scripts nbdkit-install libnbd-install nbd-kernel-module-install \
				s3backer-install s3backer-config s3backer-mount-svc-install fs-setup

## Reset all locks on the installs
reset:  check-mount-name fs-setup-reset s3backer-mount-svc-install-reset s3backer-config-reset s3backer-install-reset \
				nbd-kernel-module-install-reset libnbd-install-reset nbdkit-install-reset

## Copy the script
copy-setup-scripts:
>	@echo "[info] creating remote script dir [$(REMOTE_SCRIPT_DIR)]..."
>	@$(SSH) $(SSH_TARGET) mkdir -p $(REMOTE_SCRIPT_DIR)
>	@echo "[info] copying over scripts..."
>	@$(SCP) setup/* "$(SSH_TARGET):$(REMOTE_SCRIPT_DIR)"
>	@echo "[info] ensuring scripts are executable..."
>	@$(SSH) $(SSH_TARGET) chmod +x $(REMOTE_SCRIPT_DIR)/*.bash

# Run the libnbd-install step
libnbd-install:
> @echo "[info] installing libnbd..."
>	@$(SSH) $(SSH_TARGET) chmod +x $(REMOTE_SCRIPT_DIR)/libnbd-install.bash
>	@$(SSH) $(SSH_TARGET) -- \
	LIBNBD_RELEASE_VERSION=$(LIBNBD_RELEASE_VERSION) \
	LIBNBD_RELEASE_MAJOR_MINOR_VERSION=$(LIBNBD_RELEASE_MAJOR_MINOR_VERSION) \
	$(REMOTE_SCRIPT_DIR)/libnbd-install.bash

# Reset the lock that controls the libnbd-install step
libnbd-install-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/libnbd-$(LIBNBD_RELEASE_VERSION)-install.lock || true

# Run the nbd-kernel-install step
nbd-kernel-module-install:
> @echo "[info] installing nbd kernel module..."
>	@$(SSH) $(SSH_TARGET) -- \
	$(REMOTE_SCRIPT_DIR)/nbd-kernel-module-install.bash

# Run the lock that controls the nbd-kernel-install step
nbd-kernel-module-install-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/nbd-kernel-module-install.lock || true

# Run the nbdkit-install step
nbdkit-install:
> @echo "[info] installing nbdkit..."
>	@$(SSH) $(SSH_TARGET)  -- \
	NBDKIT_RELEASE_VERSION=$(NBDKIT_RELEASE_VERSION) \
	NBDKIT_RELEASE_MAJOR_MINOR_VERSION=$(NBDKIT_RELEASE_MAJOR_MINOR_VERSION) \
	$(REMOTE_SCRIPT_DIR)/nbdkit-install.bash

# Reset the lock that controls the nbdkit-install step
nbdkit-install-reset:
>	$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/nbdkit-$(NBDKIT_RELEASE_VERSION)-install.lock || true

# Run the s3backer-install step
s3backer-install:
> @echo "[info] installing s3backer..."
>	@$(SSH) $(SSH_TARGET)  -- \
	S3BACKER_RELEASE_VERSION=$(S3BACKER_RELEASE_VERSION) \
	$(REMOTE_SCRIPT_DIR)/s3backer-install.bash

# Reset the lock that controls the s3backer-install step
s3backer-install-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/s3backer-$(S3BACKER_RELEASE_VERSION)-install.lock || true

# Run the s3backer-config step
s3backer-config:
>	@$(SSH) $(SSH_TARGET)  -- \
	MOUNT_NAME=$(MOUNT_NAME) \
	S3_REGION=$(S3_REGION) \
	S3_BASE_URL=$(S3_BASE_URL) \
	S3_ACCESS_KEY_ID=$(B2_ACCOUNT_ID_PATH) \
	S3_SECRET_ACCESS_KEY=$(B2_KEY_PATH) \
	S3B_ENCRYPTION_KEY=$(ENCRYPTION_KEY_PATH) \
	MOUNT_S3_BUCKET_PREFIX=$(MOUNT_S3_BUCKET_PREFIX) \
	MOUNT_S3BACKER_BLOCK_CACHE_SIZE=$(MOUNT_S3BACKER_BLOCK_CACHE_SIZE) \
	$(REMOTE_SCRIPT_DIR)/s3backer-config.bash

# Reset the lock that controls the configuration for s3backer
s3backer-config-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/mount-$(MOUNT_NAME)-config.lock || true

# Run the s3backer-mount-svc-install step
s3backer-mount-svc-install:
>	@$(SSH) $(SSH_TARGET) -- \
	MOUNT_NAME=$(MOUNT_NAME) \
	MOUNT_S3_BUCKET=$(MOUNT_S3_BUCKET) \
	MOUNT_DEVICE_PATH=$(MOUNT_DEVICE_PATH) \
	MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS=$(MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS) \
	$(REMOTE_SCRIPT_DIR)/s3backer-mount-svc-install.bash

# Reset the lock that controls the s3backer-mount-svc-install step
s3backer-mount-svc-install-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/mount-$(MOUNT_NAME)-systemd-setup.lock || true

# Run the fs-setup step
fs-setup:
>	@$(SSH) $(SSH_TARGET) -- \
	MOUNT_NAME=$(MOUNT_NAME) \
	MOUNT_DEVICE_PATH=$(MOUNT_DEVICE_PATH) \
	MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS=$(MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS) \
	$(REMOTE_SCRIPT_DIR)/fs-setup.bash

# Reset the lock that controls the fs-setup step
fs-setup-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/mount-$(MOUNT_NAME)-fs-setup-and-mount.lock || true

#############
# Benchmark #
#############

bench: copy-benchmark-scripts sqlite-bench-install sqlite-bench-run
bench-reset: sqlite-bench-run-reset sqlite-bench-install-reset

## Copy the benchmark scripts
copy-benchmark-scripts:
>	@echo "[info] creating remote script dir [$(REMOTE_SCRIPT_DIR)]..."
>	@$(SSH) $(SSH_TARGET) mkdir -p $(REMOTE_SCRIPT_DIR)
>	@echo "[info] copying over scripts..."
>	@$(SCP) bench/* "$(SSH_TARGET):$(REMOTE_SCRIPT_DIR)"
>	@echo "[info] ensuring scripts are executable..."
>	@$(SSH) $(SSH_TARGET) chmod +x $(REMOTE_SCRIPT_DIR)/*.bash

# Run the sqlite-bench install step
sqlite-bench-install:
>	@$(SSH) $(SSH_TARGET) -- \
	MOUNT_NAME=$(MOUNT_NAME) \
	MOUNT_DEVICE_PATH=$(MOUNT_DEVICE_PATH) \
	MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS=$(MOUNT_SVC_STARTUP_WAIT_TIME_SECONDS) \
	$(REMOTE_SCRIPT_DIR)/sqlite-bench-install.bash

# Reset the lock that controls the fs-install step
sqlite-bench-install-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/mount-$(MOUNT_NAME)-sqlite-bench-install.lock || true

# Run the sqlite-bench run step
sqlite-bench-run:
>	@$(SSH) $(SSH_TARGET) -- \
	SQLITE_BENCH_LOCAL_DISK_DIR=$(SQLITE_BENCH_LOCAL_DISK_DIR) \
	SQLITE_BENCH_S3BACKER_WITH_BLOCK_CACHE_DIR=$(SQLITE_BENCH_S3BACKER_WITH_BLOCK_CACHE_DIR) \
	SQLITE_BENCH_S3BACKER_WITHOUT_BLOCK_CACHE_DIR=$(SQLITE_BENCH_S3BACKER_WITHOUT_BLOCK_CACHE_DIR) \
	$(REMOTE_SCRIPT_DIR)/sqlite-bench-run.bash

# Reset the lock that controls the fs-run step
sqlite-bench-run-reset:
>	@$(SSH) $(SSH_TARGET) rm /etc/experiment/progress/mount-$(MOUNT_NAME)-sqlite-bench-run.lock || true
