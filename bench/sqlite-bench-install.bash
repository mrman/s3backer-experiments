#!/usr/bin/env -S bash -eu
#
# This file installs sqlite-bench

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

########################
# sqlite-bench install #
########################

SQLITE_BENCH_RELEASE_VERSION=78e6cdc3d8791c28730f35ba0bd527d34aed2af4
SQLITE_BENCH_RELEASE_DOWNLOAD_URL=https://github.com/ukontainer/sqlite-bench/archive/${SQLITE_BENCH_RELEASE_VERSION}.tar.gz
SQLITE_BENCH_LIB_DIR=/var/lib/sqlite-bench
SQLITE_BENCH_EXPECTED_TARBALL_PATH=${SQLITE_BENCH_LIB_DIR}/sqlite-bench-${SQLITE_BENCH_RELEASE_VERSION}.tar.gz;
SQLITE_BENCH_EXPECTED_UNZIPPED_DIR=${SQLITE_BENCH_LIB_DIR}/sqlite-bench-${SQLITE_BENCH_RELEASE_VERSION}/

echo -e "===== sqlite-bench install =====";

echo "[info] installing deps...";
apt install -y sqlite;

# Install sqlite-bench (if necessary)
SQLITE_BENCH_INSTALL_LOCK=${CONFIG_PROGRESS_DIR}/sqlite-bench-install.lock
if [ -f "${SQLITE_BENCH_INSTALL_LOCK}" ]; then
  echo -e "[info] detected install lock @ [${SQLITE_BENCH_INSTALL_LOCK}], skipping sqlite-bench install...";
  exit 0;
fi

echo "[info] creating lib dir...";
mkdir -p ${SQLITE_BENCH_LIB_DIR};

echo "[info] downloading & unzipping the source code";
curl --output ${SQLITE_BENCH_EXPECTED_TARBALL_PATH} -LO ${SQLITE_BENCH_RELEASE_DOWNLOAD_URL};
cd ${SQLITE_BENCH_LIB_DIR} && tar -xf ${SQLITE_BENCH_EXPECTED_TARBALL_PATH};

echo "[info] building the project...";
pushd ${SQLITE_BENCH_EXPECTED_UNZIPPED_DIR};
make;
cp sqlite-bench /usr/bin/sqlite-bench;
popd;

echo "[info] writing lock file @ [${SQLITE_BENCH_INSTALL_LOCK}]";
date > ${SQLITE_BENCH_INSTALL_LOCK};
