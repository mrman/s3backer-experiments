#!/usr/bin/env -S bash -eu
#
# This file runs sqlite-bench against multiple folders

CONFIG_BASE_DIR=/etc/experiment
CONFIG_PROGRESS_DIR=${CONFIG_BASE_DIR}/progress

echo -e "[info] base configuration dir [${CONFIG_BASE_DIR}]...";
echo -e "[info] progress locks stored @ [${CONFIG_PROGRESS_DIR}]...";
mkdir -p ${CONFIG_PROGRESS_DIR};

#####################
# sqlite-bench runs #
#####################

echo -e "===== sqlite-bench run =====";

SQLITE_BENCH_RUN_LOCK=${CONFIG_PROGRESS_DIR}/sqlite-bench-run.lock
if [ -f "${SQLITE_BENCH_RUN_LOCK}" ]; then
  echo -e "[info] detected run lock @ [${SQLITE_BENCH_RUN_LOCK}], skipping sqlite-bench run...";
  exit 0;
fi

echo "[info] running sqlite on local disk @ [${SQLITE_BENCH_LOCAL_DISK_DIR}]...";
cd ${SQLITE_BENCH_LOCAL_DISK_DIR} && sqlite-bench;

echo "[info] removing sqlite-bench files...";
rm -rf ${SQLITE_BENCH_LOCAL_DISK_DIR}/dbbench_*.db;

echo "[info] running sqlite on s3backer w/ block-cache @ [${SQLITE_BENCH_S3BACKER_WITH_BLOCK_CACHE_DIR}]...";
cd ${SQLITE_BENCH_S3BACKER_WITH_BLOCK_CACHE_DIR} && sqlite-bench;

echo "[info] removing sqlite-bench files...";
rm -rf ${SQLITE_BENCH_S3BACKER_WITH_BLOCK_CACHE_DIR}/dbbench_*.db;

echo "[info] running sqlite on s3backer w/o block-cache @ [${SQLITE_BENCH_S3BACKER_WITHOUT_BLOCK_CACHE_DIR}]...";
cd ${SQLITE_BENCH_S3BACKER_WITHOUT_BLOCK_CACHE_DIR} && sqlite-bench --num=10;

echo "[info] removing sqlite-bench files...";
rm -rf ${SQLITE_BENCH_S3BACKER_WITHOUT_BLOCK_CACHE_DIR}/dbbench_*.db;

echo "[info] writing lock file @ [${SQLITE_BENCH_RUN_LOCK}]";
date > ${SQLITE_BENCH_RUN_LOCK};
