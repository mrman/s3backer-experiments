# S3backer plus SQLite

This experiment is to test out how well [`s3backer`][s3backer] can combine with [SQLite][sqlite].

If you're looking for *how* to install `s3backer`, check out the [Setup section](#setup)

[s3backer]: https://github.com/archiecobbs/s3backer
[sqlite]: https://sqlite.org/index.html

## Why?

Being able to run SQLite with a locally cached but massively resilient final store (S3) would be pretty useful for workloads that depend on SQLite but want to be able to more easily move from machine to machine (with various kinds of backing-storage).

[LiteFS][litefs] is probably the better choice for the *specific* SQLite use case, but **`s3backer` is a great general tool that fits in a *lot* of places, especially with [`nbd` mode](https://github.com/archiecobbs/s3backer/wiki/Network-Block-Device-(NBD)-Mode)**.

Granted workloads still have to make sure to run on only *one* instance, but it is posisble to run read-only replicas and other things with the data stored in S3.

The block caching abilities of `s3backer` also makes it really ideal for making serving data much quicker locally and mitigating some of the effects of fully remote (and possibly distant) storage.

[litefs]: https://github.com/superfly/litefs

### Hardware specs

All building and tests ran on the following hardware, but the script *should* run anywhere you have SSH access and Ubuntu 22.04 running:

- [Hetzner Cloud](https://www.hetzner.com/cloud) (in particular a CPX21 w/ 80GB "local" disk)
- [Backblaze B2 block storage](https://backblaze.com)

## Setup

Figuring out exactly how to set up `s3backer` in `nbd` mode took me quite a bit and resulted in at one [spurrious ticket](https://github.com/archiecobbs/s3backer/issues/203) which was quickly resolved and explained by [@archiecobbs](https://github.com/archiecobbs), the creator `s3backer`.

Generally on a fresh Ubuntu 22.04 you need to do the following:

- Install a newer version of [`nbdkit`][nbdkit] (as of 2023/01/20, the version that comes with Ubuntu 22.04 will *not* auto-enable the `nbd` functionality when building `s3backer`)
- Install [`libnbd`][libnbd]
- Install `nbd` as a kernel module (note: without this, you will have no `/dev/nbdX` drives)
- Install [`s3backer`][s3backer]
- Configure `s3backer`
- Create a [SystemD service][systemd-service] that ensures `s3backer` starts automatically for the given bucket/disk/mount
- Mount & configure `/etc/fstab` for the `s3backer` disks

In `Makefile`, that translates to:

```
## Set up a mount by running the script on the remote servers
setup:  check-mount-name copy-setup-scripts nbdkit-install libnbd-install nbd-kernel-module-install \
				s3backer-install s3backer-config s3backer-mount-svc-install fs-setup
```

You can find the contents of all these steps and the scripts that make them completely automated in [`setup/`](./setup), and the detail of how to pipe these things together in [`Makefile`](./Makefile).

Translating the scripts into [Ansible][ansible] playbooks is an exercise left for the reader.

[ansible]: https://docs.ansible.com
[nbdkit]: https://github.com/libguestfs/nbdkit
[libnbd]: https://gitlab.com/nbdkit/libnbd

### Hypotheses

A few things that we'll be confirming

- `sqlite` can run properly on a system mounted on `s3backer`
- `sqlite` will see less than 20% degradation in performance for
- `sqlite` will be easy to tear down and restart based on the instance backed by s3backer

Use cases we'll be looking at:

- high data write workload
- high data read workload

It's important to know how this set up fails (network issues, bucket removed/moved?), so we'll try to cover that too.

### Prior Art

Turns out someone has already done [exactly this -- ZFS on `s3backer`](https://github.com/archiecobbs/s3backer/wiki/Case-Study:-Setting-up-NAS-using-ZFS-over-s3backer).

### Interesting points of experimentation

- [We'll be using NBD mode](https://github.com/archiecobbs/s3backer/wiki/Network-Block-Device-(NBD)-Mode) (which allows us to skip FUSE)

## Running the experiment

To run the experiments:

```console
export SSH_TARGET=root@<node>
export MOUNT_NAME=main
export MOUNT_DEVICE_PATH=/dev/nbd0
export S3_REGION=eu-central-003
export S3_BASE_URL=https://s3.eu-central-003.backblazeb2.com
make
```

NOTE: the ENV vars above are not *all necessary* (defaults are usually fine) -- see `Makefile` for the complete listing.

The default make target (`all`) will set up a given machine (`SSH_TARGET`) *and* run benchmarks.

After the machine is set up, you can run [`sqlite-bench`][sqlite-bench] to test performance:

[sqlite-bench]: https://github.com/ukontainer/sqlite-bench

## Results

To obtain results, we run `bench-sqlite` on a variety of setups available on the remote machine:

- Local FS (using a regular fine on the local filesystem)
- `s3backer` in NBD mode with a block cache set
- `s3backer` in NBD mode without a block cache set (so every FS write is network)

With respect to the hypotheses:

- ✅ `sqlite` can run properly on a system mounted on `s3backer`
  - As promised by `s3backer` docs, SQLite *was* easy to run (no need to worry about issues), and would likely be even safer with WAL mode enabled.
- ❌ `sqlite` will see less than 20% degradation in performance for
  - While *most* kinds of writes didn't see degradation (due to block cache), there was a ~20x+ increase in `fillseqsync` latency (as one might expect) as writes would go through. This *was* drastically better than a full network-only sync run though.
- ✅ `sqlite` will be easy to tear down and restart based on the instance backed by s3backer
  - While it wasn't easy to build the automation, once in place the machine could easy stop and restart.
  - Failure modes need to be researched some more.

Surprising to no-one, trying to run a database test on a synchronous network drive performs *very poorly*, but the fact that the system is relatively usable with a persistent block-cache for sequential write is pretty interesting and certainly worth noting.

Many types of benchmark results were virtually unchanged, and it's likely that this data would only get *better* with larger data sizes and more aggressive batching (`sqlite-bench` is a database test after all).

### Benchmark: `dd`

Running simple `dd` based test:

```console
# this will write 1GB of random data to disk
dd bs=1024 count=1048576 </dev/urandom >random.data # 1k blocks, 1GB written
rm -rf random.data # clearing data set in-between to ensure pure write workload

dd bs=131072 count=8192 </dev/urandom >random.data # 128k blocks, ~1GB written
rm -rf random.data # clearing data set in-between to ensure pure write workload

dd bs=1048576 count=1024 </dev/urandom >random.data # 1M blocks, ~1GB written
rm -rf random.data # clearing data set in-between to ensure pure write workload
```

NOTE: these tests weren't automated -- I performed them manually after setup. It was enough to go into different directories (mounted/unmounted) and run the commands above.

NOTE: bandwidth for the kind of Hetzner isn't *too* high (especially to a target as far away as Backblaze EU) -- I got consistently around 2.5-3MBps (note the "B") of network traffic -- massively under-utilizing the network (and of course the disks).

### At a glance

| Workload          | `dd` metric          | local   | `s3backer`/nbd cached | `s3backer`/nbd - sync |
|-------------------|----------------------|---------|-----------------------|-----------------------|
| 1k blocks, 1GB    | Throughput (MB/s)    | `202`   | `5.9`                 | `1.6`                 |
| 128k blocks, ~1GB | Throughput (MB/s)    | `275`   | `5.9`                 | `1.6`                 |
| 1M blocks, ~1GB   | Throughput (MB/s)    | `274`   | `5.8`                 | `1.6`                 |
| 1k blocks, 1GB    | Total time (seconds) | `5.306` | `180.597`             | `667.152`             |
| 128k blocks, ~1GB | Total time (seconds) | `3.899` | `181.844`             | `656.275`             |
| 1M blocks, ~1GB   | Total time (seconds) | `3.916` | `184.376`             | `661.571`             |

So clearly, a 40x decrease in performance for just a plain write of lots of data. It's a bit surprising that the cached version did so poorly (it's performance should have been close to local, given the block cache)Considering a spinning rust hard drive writes way faster than this, clearly writing to a spinning disk is still way faster.

There is also the possibility that I'm running into noisy neighbors (this instance isn't dedicated), but for my purposes at least having the relative numbers is helpful.

#### Raw output of DD run on local disk

```
root@s3backer:~# dd bs=1024 count=1048576 </dev/urandom >random.data
1048576+0 records in
1048576+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 5.30556 s, 202 MB/s

root@s3backer:~# dd bs=131072 count=8192 </dev/urandom >random.data # 128k blocks, ~1GB written
8192+0 records in
8192+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 3.89918 s, 275 MB/s

root@s3backer:~# dd bs=1048576 count=1024 </dev/urandom >random.data # 1M blocks, ~1GB written
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 3.91648 s, 274 MB/s
```

#### Raw output for `s3backer` w/ block cache

```
root@s3backer:/mnt/s3backer/cached# dd bs=1024 count=1048576 </dev/urandom >random.data  # 1k blocks, 1GB written
1048576+0 records in
1048576+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 180.597 s, 5.9 MB/s

root@s3backer:/mnt/s3backer/cached# dd bs=131072 count=8192 </dev/urandom >random.data # 128k blocks, ~1GB written
8192+0 records in
8192+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 181.844 s, 5.9 MB/s

root@s3backer:/mnt/s3backer/cached# dd bs=1048576 count=1024 </dev/urandom >random.data # 1M blocks, ~1GB written
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 184.376 s, 5.8 MB/s
```

#### Raw output for `s3backer` w/o block cache

Remember, no block cache means that every write is remote access, essentially!

```
root@s3backer:/mnt/s3backer/sync# dd bs=1024 count=1048576 </dev/urandom >random.data # 1k blocks, 1GB written
1048576+0 records in
1048576+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 667.152 s, 1.6 MB/s

root@s3backer:/mnt/s3backer/cached# dd bs=131072 count=8192 </dev/urandom >random.data # 128k blocks, ~1GB written
8192+0 records in
8192+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 656.275 s, 1.6 MB/s

root@s3backer:/mnt/s3backer/cached# dd bs=1048576 count=1024 </dev/urandom >random.data # 1M blocks, ~1GB written
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 661.571 s, 1.6 MB/s
```

Graphs while it was running (first case of 1k blocks):

![Hetzner cloud graphs during s3backer copy](./images/cpx21-s3backer-sync-workload-graphs.png)

### Benchmark: `bench-sqlite`

### At a glance

After running `bench-sqlite` the results on perf are as follows:

| `sqlite-bench` benchmark name | Local FS (us/op) | `s3backer`/nbd w/ block cache (us/op) | `s3backer`/nbd w/o block cache (us/op, 1/100,000 work) |
|-------------------------------|------------------|---------------------------------------|--------------------------------------------------------|
| fillseq                       | `8.268`          | `8.958`                               | `38.099`                                               |
| fillseqsync                   | `292.599`        | `7502.182`                            | `4846094.847`                                          |
| fillseqbatch                  | `1.619`          | `1.578`                               | `3.866`                                                |
| fillrandom                    | `14.045`         | `24.132`                              | `107.503`                                              |
| fillrandsync                  | `332.681`        | `5524.710`                            | `9406972.170`                                          |
| fillrandbatch                 | `9.288`          | `8.754`                               | `2.578`                                                |
| overwrite                     | `18.884`         | `30.273`                              | `52.691`                                               |
| overwritebatch                | `15.984`         | `19.150`                              | `1.805`                                                |
| readrandom                    | `4.624`          | `4.467`                               | `37.408`                                               |
| readseq                       | `3.123`          | `2.984`                               | `24.295`                                               |
| fillrand100K                  | `214.916`        | `224.627`                             | `641.823`                                              |
| fillseq100K                   | `221.614`        | `243.406`                             | `818.014`                                              |
| readseq                       | `0.701`          | `0.724`                               | `51.117`                                               |
| readrand100K                  | `28.570`         | `30.458`                              | `405.788`                                              |

#### Raw output for local disk

```
===== sqlite-bench run =====
[info] running sqlite on local disk @ [/home/root]...
SQLite:     version 3.25.2
Date:       Fri Jan 20 08:11:03 2023
CPU:        3 * AMD EPYC Processor
CPUCache:   512 KB
Keys:       16 bytes each
Values:     100 bytes each
Entries:    1000000
RawSize:    110.6 MB (estimated)
------------------------------------------------
fillseq      :       8.268 micros/op;
fillseqsync  :     292.599 micros/op;
fillseqbatch :       1.619 micros/op;
fillrandom   :      14.045 micros/op;
fillrandsync :     332.681 micros/op;
fillrandbatch :       9.288 micros/op;
overwrite    :      18.884 micros/op;    5.9 MB/s
overwritebatch :      15.984 micros/op;
readrandom   :       4.624 micros/op;
readseq      :       3.123 micros/op;
fillrand100K :     214.916 micros/op;  44
fillseq100K  :     221.614 micros/op;  43
readseq      :       0.701 micros/op;
readrand100K :      28.570 micros/op;
```

#### Raw output for `s3backer` w/ block cache

```
[info] running sqlite on s3backer w/ block-cache @ [/mnt/s3backer/cached]...
/etc/s3backer/scripts/sqlite-bench-run.bash: line 25: cd: /home/mrman: No such file or directory
SQLite:     version 3.25.2
Date:       Fri Jan 20 06:02:07 2023
CPU:        3 * AMD EPYC Processor
CPUCache:   512 KB
Keys:       16 bytes each
Values:     100 bytes each
Entries:    1000000
RawSize:    110.6 MB (estimated)
------------------------------------------------
fillseq      :       8.958 micros/op;
fillseqsync  :    7502.183 micros/op;
fillseqbatch :       1.578 micros/op;
fillrandom   :      24.132 micros/op;
fillrandsync :    5524.710 micros/op;
fillrandbatch :       8.754 micros/op;
overwrite    :      30.273 micros/op;    3.7 MB/s
overwritebatch :      19.150 micros/op;
readrandom   :       4.467 micros/op;
readseq      :       2.984 micros/op;
fillrand100K :     224.627 micros/op;  42
fillseq100K  :     243.406 micros/op;  39
readseq      :       0.724 micros/op;
readrand100K :      30.458 micros/op;
```

#### Raw output for `s3backer` w/o block cache (`--num=10`, 1/100,000 work)

These runs take a *long* time, as you might imagine, so much so that I never finished it (the first time). Note that `sqlite-bench` was set to `--num=10` (versus the default of `1,000,000`!) just to be sure tha the runs would *complete at all*.

Not using a buffer cache means a network request for every single FS operation, so I had to *drastically* reduce the number of "entries" from 1,000,000 to 100.

```
===== sqlite-bench run =====
[info] running sqlite on s3backer w/o block-cache @ [/mnt/s3backer/sync]...
SQLite:     version 3.25.2
Date:       Fri Jan 20 08:20:13 2023
CPU:        3 * AMD EPYC Processor
CPUCache:   512 KB
Keys:       16 bytes each
Values:     100 bytes each
Entries:    10
RawSize:    0.0 MB (estimated)
------------------------------------------------
fillseq      :      38.099 micros/op;
fillseqsync  : 4846094.847 micros/op; (0 ops)
fillseqbatch :       3.866 micros/op;
fillrandom   :     107.503 micros/op;
fillrandsync : 9406972.170 micros/op; (0 ops)
fillrandbatch :       2.578 micros/op;
overwrite    :      52.691 micros/op;
overwritebatch :       1.805 micros/op;
readrandom   :      37.408 micros/op;
readseq      :      24.295 micros/op;
fillrand100K :     641.823 micros/op; (0 ops)
fillseq100K  :     818.014 micros/op; (0 ops)
readseq      :      51.117 micros/op;
readrand100K :     405.788 micros/op;
```

**Obviously, these are terrible results for a database**, but using `s3backer` without a block cache might be great for other use cases that are less small writes and more larger writes w/ more sporadic batching where async is OK.
